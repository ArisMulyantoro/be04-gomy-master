package hGuest

import (
	"be04gomy/handler"
	"be04gomy/model/mPermintaan"
	"encoding/json"
	"net/http"
	"strconv"
)

func PermintaanList(ctx *handler.Ctx) {
	permintaans, err := mPermintaan.SelectAll(ctx.Db)
	if ctx.IsError(err) {
		return
	}
	ctx.End(permintaans)
}

func PermintaanCreate(ctx *handler.Ctx) {
	if ctx.Request.Method == `GET` {
		http.ServeFile(ctx, ctx.Request, ctx.ViewsDir+`guest/Permintaan barang_create.html`)
		return
	}
	m := mPermintaan.Permintaan{}
	err := json.NewDecoder(ctx.Request.Body).Decode(&m)
	if ctx.IsError(err) {
		return
	}
	err = mPermintaan.Insert(ctx.Db, &m)
	if ctx.IsError(err) {
		return
	}
	ctx.End(m)
}

func PermintaanUpdate(ctx *handler.Ctx) {
	if ctx.Request.Method == `GET` {
		http.ServeFile(ctx, ctx.Request, ctx.ViewsDir+`guest/Permintaan barang_update.html`)
		return
	}
	m := mPermintaan.Permintaan{}
	err := json.NewDecoder(ctx.Request.Body).Decode(&m)
	if ctx.IsError(err) {
		return
	}
	err = mPermintaan.Update(ctx.Db, &m)
	if ctx.IsError(err) {
		return
	}
	ctx.End(m)
}

//ARIS MULYANTORO DELETE
func PermintaanDelete(ctx *handler.Ctx) {
	if ctx.Request.Method == `DELETE` {
		emp := ctx.Request.URL.Query().Get("id")
		i, err := strconv.Atoi(emp)
		err = mPermintaan.Delete(ctx.Db, &i)
		if ctx.IsError(err) {
			return
		}
		rs := map[string]bool{
			"success": true,
		}
		ctx.End(rs)
	}
	return
}