package mPermintaan

import (
	"database/sql"
	"errors"
	"fmt"
	"time"
)

type Permintaan struct {
	Id        int64  `json:"id,string"`
	Nama       string // `json:"nim"`
	Divisi      string
    Nama_barang  string
	CreatedAt time.Time
	UpdatedAt time.Time
}

const dateFormat = `2006-01-02 15:04:05`

func SelectAll(db *sql.DB) (permintaans []Permintaan, err error) {
	rows, err := db.Query(`SELECT * FROM permintaan ORDER BY id DESC`)
	permintaans = []Permintaan{}
	if err != nil {
		return permintaans, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Permintaan{}
		createdAt, updatedAt := ``, ``
		err = rows.Scan(
			&s.Id,
			&s.Nama,
			&s.Divisi,
			&s.Nama_barang,
			&createdAt,
			&updatedAt)
		if err != nil {
			return
		}
		s.CreatedAt, _ = time.Parse(dateFormat, createdAt)
		s.UpdatedAt, _ = time.Parse(dateFormat, updatedAt)
		permintaans = append(permintaans, s)
	}
	return
}

func Insert(db *sql.DB, m *Permintaan) (err error) {
	now := time.Now()
	res, err := db.Exec(`INSERT INTO permintaan(nama,divisi,nama_barang,created_at,updated_at)
VALUES(?,?,?,?,?)`,
		m.Nama,
		m.Divisi,
		m.Nama_barang,
		now,
		now)
	if err != nil {
		return err
	}
	m.Id, err = res.LastInsertId()
	if err == nil {
		m.CreatedAt = now
		m.UpdatedAt = now
	}
	return err
}

func Update(db *sql.DB, m *Permintaan) (err error) {
	keys := []string{}
	values := []interface{}{}
	if m.Divisi != ``{
		keys = append(keys, `divisi`)
		values = append(values, m.Divisi)
	}
	if m.Nama != `` {
		keys = append(keys, `nama`)
		values = append(values, m.Nama)
	}
	if m.Nama_barang != `` {
		keys = append(keys, `nama_barang`)
		values = append(values, m.Nama_barang)
	}
	sql := `UPDATE permintaan SET `
	for idx, key := range keys {
		if idx > 0 {
			sql += `,`
		}
		sql += key + `= ?`
	}
	sql += ` ,updated_at = ? WHERE id = ?`
	now := time.Now()
	values = append(values, now, m.Id)
	res, err := db.Exec(sql, values...)
	if err != nil {
		return err
	}
	check, err := res.RowsAffected()
	fmt.Println(check)
	if err == nil {
		m.UpdatedAt = now
	}
	return err
}

//ARIS MULYANTORO DELETE
func Delete(db *sql.DB, m *int) (err error) {
	res, err := db.Exec(`DELETE FROM permintaan WHERE id = ?`, m)
	if err != nil {
		return err
	}
	check, err := res.RowsAffected()
	fmt.Println("ID ", check, " Deleted")
	if check == 0 {
		return errors.New("Missing ID")
	}
	return err
}
