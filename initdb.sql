CREATE DATABASE Permintaan_Barang;
USE Permintaan_Barang;
CREATE USER `user04`@`localhost` IDENTIFIED BY '1234567';
GRANT ALL PRIVILEGES ON Permintaan_Barang TO `user04`@`localhost`;
FLUSH PRIVILEGES; 
-- mysql -u user04 -p be04

CREATE TABLE Permintaan_Barang (
	`id` INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	`nama` VARCHAR(12) NOT NULL,
	`divisi` VARCHAR(64) NOT NULL,
	`nama barang` SMALLINT NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL
);
