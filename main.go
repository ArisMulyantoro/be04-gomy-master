package main

import (
	"be04gomy/handler"
	"be04gomy/handler/hGuest"
)

const PORT = `:7041`
func main() {
	server := handler.InitServer(`views/`)
	server.Handle(`/guest/permintaan/list`,hGuest.PermintaanList)
	server.Handle(`/guest/permintaan/create`,hGuest.PermintaanCreate)
	server.Handle(`/guest/permintaan/update`,hGuest.PermintaanUpdate)
	server.Handle(`/guest/permintaan/delete`,hGuest.PermintaanDelete)
	server.Listen(PORT) 
}
